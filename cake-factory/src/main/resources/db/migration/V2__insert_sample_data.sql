delete from CAKE where id < 7;
insert into CAKE (id,created,updated,version,shortcut,name,price) values (1,now(),now(),0,'abcr', 'Butter Croissant', 0.75);
insert into CAKE (id,created,updated,version,shortcut,name,price) values (2,now(),now(),0,'ccr', 'Schoko Croissant', 0.95);
insert into CAKE (id,created,updated,version,shortcut,name,price) values (3,now(),now(),0,'b', 'Baguette', 1.60);
insert into CAKE (id,created,updated,version,shortcut,name,price) values (4,now(),now(),0,'rv', 'Roter Samtkuchen', 3.95);
insert into CAKE (id,created,updated,version,shortcut,name,price) values (5,now(),now(),0,'vs', 'Biskuit Kuchen', 5.45);
insert into CAKE (id,created,updated,version,shortcut,name,price) values (6,now(),now(),0,'cc', 'Rübli Kuchen', 3.45);
