package com.gerdmueller.cakefactory.ui;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.servlet.ModelAndView;

import com.gerdmueller.cakefactory.app.CatalogJpaService;

@Controller
public class CatalogController {
	
	@Autowired
	private CatalogJpaService dataService;
	
//	@ModelAttribute
//	public void addCakesToModel(Model model) {
//		model.addAttribute("cakes", dataService.getAll());
//	}
	
    @GetMapping("/")
    ModelAndView index() {
        return new ModelAndView("home", Map.of("cakes", this.dataService.getAll()));
    }	

}
