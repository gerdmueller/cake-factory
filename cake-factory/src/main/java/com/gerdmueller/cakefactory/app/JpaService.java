package com.gerdmueller.cakefactory.app;

import java.util.Optional;

import com.gerdmueller.cakefactory.persistence.JpaEntity;

public interface JpaService {

	Optional<? extends JpaEntity> getById(Long id);
	public Iterable<? extends JpaEntity> getAll();
	JpaEntity save(JpaEntity entity);
	void deleteById(Long id);

}
