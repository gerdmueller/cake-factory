package com.gerdmueller.cakefactory.app;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gerdmueller.cakefactory.persistence.CakeJpaEntity;
import com.gerdmueller.cakefactory.persistence.CakeRepository;
import com.gerdmueller.cakefactory.persistence.JpaEntity;

@Service
public class CatalogJpaService implements JpaService {
	
	@Autowired
	CakeRepository cakeRepository;

	@Override
	public Optional<CakeJpaEntity> getById(Long id) {		
		return cakeRepository.findById(id);
	}

	@Override
	public List<CakeJpaEntity> getAll() {
		
        return StreamSupport.stream(cakeRepository.findAll().spliterator(), false)
                .map(entity -> new CakeJpaEntity(entity.getShortcut(),entity.getName(),entity.getPrice()))
                .collect(Collectors.toList());		
		
	}

	@Override
	public CakeJpaEntity save(JpaEntity i) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void deleteById(Long id) {
		// TODO Auto-generated method stub
		cakeRepository.deleteById(id);
	}

}
