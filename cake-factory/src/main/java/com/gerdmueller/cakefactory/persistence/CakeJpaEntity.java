package com.gerdmueller.cakefactory.persistence;

import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

import org.hibernate.annotations.NaturalId;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "cake")
@Getter @Setter @ToString @NoArgsConstructor
public class CakeJpaEntity extends JpaEntity {
	
    @NaturalId
    @Column(nullable = false, unique = true)	
	private String shortcut;
    
	private String name;
	private BigDecimal price;
	
	public CakeJpaEntity(String shortcut, String name, BigDecimal price) {
		super();
		this.shortcut = shortcut;
		this.name = name;
		this.price = price;
	}	

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CakeJpaEntity)) return false;
        CakeJpaEntity other= (CakeJpaEntity) o;
        return Objects.equals(shortcut, other.getShortcut());
    }
 
    @Override
    public int hashCode() {
        return Objects.hash(shortcut);
    }	
	
}
