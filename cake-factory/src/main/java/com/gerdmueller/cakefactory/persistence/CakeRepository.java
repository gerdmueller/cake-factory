package com.gerdmueller.cakefactory.persistence;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CakeRepository extends CrudRepository<CakeJpaEntity,Long> {
	
}
