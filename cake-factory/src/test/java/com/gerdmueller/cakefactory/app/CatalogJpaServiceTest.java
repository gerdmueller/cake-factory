package com.gerdmueller.cakefactory.app;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;
import org.springframework.test.context.jdbc.Sql;

// DataJpaTest lädt JpaEntities und JpaRepositories
// da auch der Service getestet werden soll
// muss der händisch zum Kontext hinzugefügt werden
// sonst geht das Autowiring nicht
@DataJpaTest
@ComponentScan(includeFilters = @ComponentScan.Filter(type = FilterType.REGEX, pattern = ".*[CatalogJpaService]"))
@Sql("/test_content.sql")
public class CatalogJpaServiceTest {    	
	
	@Autowired
	private CatalogJpaService dataService;
	
	// TestEntityManager für DB-Operationen in DataJpaTest
//    @Autowired
//    private TestEntityManager testEntityManager;	
	
//	@MockBean
//	private CakeRepository cakeRepository;
//	
//	@BeforeEach
//	public void setUp() {
//	 
//	    Mockito.when(cakeRepository.findAll())
//	    	.thenReturn(
//	    			Arrays.asList(
//					new CakeJpaEntity("kk","Käsekuchen",BigDecimal.valueOf(2.15))
//					,new CakeJpaEntity("ak","Apfelkuchen",BigDecimal.valueOf(2.25))
//					,new CakeJpaEntity("bs","Belgischer Schokokuchen",BigDecimal.valueOf(2.05))
//					,new CakeJpaEntity("sk","Schwarzwälder Kirsch",BigDecimal.valueOf(2.50))
//					,new CakeJpaEntity("fk","Frankfurter Kranz",BigDecimal.valueOf(2.35))
//					,new CakeJpaEntity("dw","Donauwelle",BigDecimal.valueOf(1.95))	    			
//	    			));
//
//	}
	
	@Test
	@DisplayName("Tests the service layer")
	public void TestGetCakeData() {

		assertThat(dataService.getAll()).hasSize(6);
		
	}

}

