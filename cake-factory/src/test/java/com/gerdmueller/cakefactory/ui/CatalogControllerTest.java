package com.gerdmueller.cakefactory.ui;

import org.hamcrest.Matchers;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.gerdmueller.cakefactory.app.CatalogJpaService;

@WebMvcTest(CatalogController.class)
class CatalogControllerTest {

    @Autowired
    private MockMvc mockMvc;	
	
	@MockBean 
	CatalogJpaService dataService;
	

	@Test
	@DisplayName("Checks correct view and title of landing page (MockMvc)")
	public void testHomepage() throws Exception {
		
		mockMvc.perform(MockMvcRequestBuilders.get("/"))
		.andExpect(MockMvcResultMatchers.status().isOk())
		.andExpect(MockMvcResultMatchers.view().name("home"))
		.andExpect(MockMvcResultMatchers.content().string(Matchers.containsString("Cake Factory Homepage")));
		
	}
}
